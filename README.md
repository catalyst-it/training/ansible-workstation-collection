# Ansible Collection - training.workstation

Documentation for the collection.


See roles for specific documentation on usage:

## Roles

* [openstack_authentication](roles/openstack_authentication/README.md)
* [user_ssh_key](roles/user_ssh_key/README.md)
* [vagrant](roles/vagrant/README.md)
* [vim](roles/vim/README.md)
* [vim_yaml](roles/vim_yaml/README.md)
