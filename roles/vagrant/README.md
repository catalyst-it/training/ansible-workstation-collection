vagrant
=========

Install vagrant on training workstations

Requirements
------------

none

Role Variables
--------------

## `vagrant_images`

List of vagrant images to download. Defaults to

```
['centos/7', 'ubuntu/focal64']
```

Dependencies
------------

none

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - { role: training.workstation.vagrant, vagrant_images: ['ubuntu/xenial64', 'ubuntu/trusty64'] }

License
-------

BSD

Author Information
------------------

Travis at Catalyst
