user_ssh_key
=========

Set up a SSH key for _train_ user on workstations

Requirements
------------

none

Role Variables
--------------

none

Dependencies
------------

none

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - { role: training.workstation.user_ssh_key }

License
-------

BSD

Author Information
------------------

Travis at Catalyst
