openstack_authentication
=========

Set up OpenStack authentication on workstations

Requirements
------------

* An Openstack tenant and valid credentials

Role Variables
--------------

| Variable          | Description   | Default Value |
| ---   | --- | ---   |
|`openstack_config_directory`  | Directory to create clouds config   |     `$HOME/.config/openstack`|
|`os_auth_url`     | URL of OpenStack Authentication server  |    https://api.cloud.catalyst.net.nz:5000/v3|
|`os_user_domain_name`     |  Openstack domain |   Default|
|`os_project_domain_name`     |  Openstack project |   Default|
|`os_region_name`     | OpenStack region |    nz-por-1|
|`os_interface`     |  OpenStack interface |   public|
|`cloud_name`     |  Cloud to use  | `openstack` |
|`profile_name`     | Name of profile to add to clouds.yaml | `catalystcloud` |
|`os_project_name`     |  Openstack project |   |
| `os_password`  | Password for openstack tenant |  |
| `os_username`  | User name for openstack tenant |  |


Dependencies
------------

none

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - training.workstation.openstack_authentication
           os_username: myaccount@catalyst.net.nz
           os_project_name: projectname
           os_password: "{{ vault_os_password }}"

License
-------

BSD

Author Information
------------------

Travis at Catalyst
